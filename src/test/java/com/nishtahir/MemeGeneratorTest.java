package com.nishtahir;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

public class MemeGeneratorTest {

    @Test(expected = FileNotFoundException.class)
    public void generateMeme_WithInvalidPath_ThrowsException() throws Exception {
        MemeGenerator.generateMeme("", "", "", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateMeme_WithInvalidFile_ThrowsException() throws Exception {
        MemeGenerator.generateMeme(new File("src/test/resources/dummy.txt"), new File(""), "", "");
    }

    @Test
    public void generateMeme_WithValidFile_WorksCorrectly() throws Exception {
        File input = new File("src/test/resources/test.jpg");
        File output = new File("src/test/resources/output.jpg");
        output.delete();
        MemeGenerator.generateMeme(input, output, "It's a", "Java memegen");
        assertTrue(output.exists());
        output.delete();
    }

    @Test
    public void getImageFormUrl() throws Exception{

    }

}