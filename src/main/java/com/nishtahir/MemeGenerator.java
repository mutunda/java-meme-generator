package com.nishtahir;

import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;

import javax.imageio.ImageIO;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * Small library to generate memes.
 */
public class MemeGenerator {

    /**
     *
     */
    private static final int STROKE_WIDTH = 3;

    /**
     *
     */
    private static final int FONT_SCALE_FACTOR = 5;

    /**
     * Width of the source image.
     */
    private int width;

    /**
     * Height of the source image.
     */
    private int height;

    /**
     *
     */
    private Graphics2D g2d;

    /**
     *
     */
    private Font font;

    /**
     *
     */
    private FontMetrics fontMetrics;

    /**
     *
     */
    private AffineTransform origin;

    /**
     *
     */
    private static ContentInfoUtil contentInfoUtil;

    static {
        contentInfoUtil = new ContentInfoUtil();
    }

    /**
     * @param file       Image file
     * @param topText    text to appear at the top of the image.
     * @param bottomText text to appear at the bottom of the image
     * @return Meme-ified image.
     * @throws IOException Nobody ever expects the spanish inquizition.
     */
    private BufferedImage createMeme(final File file, final String topText,
                                     final String bottomText) throws IOException {

        Image image = ImageIO.read(file);
        return getBufferedImage(topText, bottomText, image);
    }

    private BufferedImage createMeme(final URL url, final String topText,
                                     final String bottomText) throws IOException {
        BufferedImage image = ImageIO.read(url);
        return getBufferedImage(topText, bottomText, image);
    }


    private BufferedImage getBufferedImage(String topText, String bottomText, Image image) {
        width = image.getWidth(null);
        height = image.getHeight(null);
        int fontSize = height / getFontScaleFactor();

        BufferedImage renderedImage = new BufferedImage(
                width, height, BufferedImage.TYPE_INT_RGB);
        g2d = renderedImage.createGraphics();

        origin = g2d.getTransform();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.drawImage(image, 0, 0, null);

        font = new Font("Impact", Font.BOLD, fontSize);
        fontMetrics = g2d.getFontMetrics(font);

        drawStringAsTopText(topText.toUpperCase());
        drawStringAsBottomText(bottomText.toUpperCase());

        g2d.dispose();
        return renderedImage;
    }


    /**
     * @param topText text to appear at the top of the image.
     */
    protected void drawStringAsTopText(final String topText) {
        Shape outline = getShapeOutlineFromString(topText);

        int x = (width - outline.getBounds().width) / 2;
        int y = fontMetrics.getAscent();

        drawStrokedShapeAt(outline, x, y);
    }

    /**
     * @param bottomText text to appear at the bottom of the image.
     */
    protected void drawStringAsBottomText(final String bottomText) {
        Shape outline = getShapeOutlineFromString(bottomText);

        int x = (width - outline.getBounds().width) / 2;
        int y = height - fontMetrics.getDescent();

        drawStrokedShapeAt(outline, x, y);
    }

    /**
     * @param shape Shape to draw.
     * @param x     Horizontal axis origin to draw.
     * @param y     Vertical axis origin to draw.
     */
    protected void drawStrokedShapeAt(final Shape shape, final int x,
                                      final int y) {
        g2d.translate(x, y);
        g2d.setColor(Color.WHITE);
        g2d.fill(shape);
        g2d.setStroke(new BasicStroke(getStrokeWidth()));
        g2d.setColor(Color.BLACK);
        g2d.draw(shape);
        g2d.setTransform(origin);
    }

    /**
     * @param text Text to render outline
     * @return Outline for text
     */
    protected Shape getShapeOutlineFromString(final String text) {
        FontRenderContext frc = g2d.getFontRenderContext();
        TextLayout textTl = new TextLayout(text, font, frc);
        return textTl.getOutline(null);
    }

    /**
     * @param imageFile  Reference to image file
     * @param outputFile Reference to output location of meme.
     * @param topText    text to appear at the top of the image.
     * @param bottomText text to appear at the bottom of the image.
     * @return reference to meme-ified image.
     * @throws IOException if file is not found or is not an image.
     */
    public static File generateMeme(final File imageFile, final File outputFile,
                                    final String topText, final String bottomText) throws IOException {
        if (!imageFile.exists()) {
            throw new FileNotFoundException();
        }

        ContentInfo info = contentInfoUtil.findMatch(imageFile);
        if (info == null || info.getMimeType() == null
                || !info.getMimeType().toLowerCase().contains("image")) {
            throw new IllegalArgumentException(
                    "Input file needs to be an image");
        }

        BufferedImage image = new MemeGenerator().createMeme(imageFile,
                topText, bottomText);
        ImageIO.write(image, "jpg", outputFile);
        return outputFile;
    }

    /**'
     * @param path       path to image.
     * @param outputPath output location of meme.
     * @param topText    text to appear at the top of the image.
     * @param bottomText text to appear at the bottom of the image
     * @return reference to meme-ified image.
     * @throws IOException if file is not found or is not an image.
     */
    public static File generateMeme(final String path, final String outputPath,
                                    final String topText, final String bottomText) throws IOException {
        return generateMeme(new File(path), new File(outputPath),
                topText, bottomText);
    }

    /**
     * @return Stroke width for meme text.
     */
    public static int getStrokeWidth() {
        return STROKE_WIDTH;
    }

    /**
     * @return Font size scale factor
     */
    public static int getFontScaleFactor() {
        return FONT_SCALE_FACTOR;
    }

}
